const mongoose = require('mongoose');

const movieVoteSchema = new mongoose.Schema({
  user_id: {
    type: String,
  },
  vote: {
    type: Number,
    min: 0,
    max: 4,
  },
});

exports.schema = movieVoteSchema;

module.exports = mongoose.model('Vote', movieVoteSchema);
