const mongoose = require('mongoose');
const uuid = require('uuid');
const MovieVote = require('./MovieVote');

const movieSchema = new mongoose.Schema({
  uuid: {
    type: String,
    default: uuid.v4,
    unique: true,
  },
  name: {
    type: String,
    required: true,
    unique: true,
    lowercase: true,
  },
  synopsis: {
    type: String,
    required: true,
  },
  director: {
    type: String,
    required: true,
  },
  genre: {
    type: String,
    required: true,
  },
  actors: [{ type: String }],
  votes: [MovieVote.schema],
}, { timestamps: true });

movieSchema.statics = {
  async getMovieById(id) {
    return this.findOne({ uuid: id });
  },
};

module.exports = mongoose.model('Movie', movieSchema);
