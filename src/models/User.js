const mongoose = require('mongoose');
const mongooseDelete = require('mongoose-delete');
const uuid = require('uuid');
const bcrypt = require('bcrypt-nodejs');

const userSchema = new mongoose.Schema({
  uuid: {
    type: String,
    default: uuid.v4,
    unique: true,
  },
  email: {
    type: String,
    required: true,
    unique: true,
    lowercase: true,
  },
  password: {
    type: String,
    required: true,
    minlength: 4,
    maxlength: 128,
  },
  name: {
    type: String,
    maxlength: 50,
  },
  role: {
    type: String,
    default: 'user',
    enum: ['user', 'admin'],
  },
}, { timestamps: true });

userSchema.plugin(mongooseDelete, { deletedAt: true });

userSchema.pre('save', async function save(next) {
  try {
    if (!this.isModified('password')) {
      return next();
    }

    this.password = bcrypt.hashSync(this.password);

    return next();
  } catch (error) {
    return next(error);
  }
});

userSchema.statics = {
  getUserById(id) {
    return this.findOne({ role: 'user', uuid: id });
  },
  getUserByEmail(email) {
    return this.findOne({ role: 'user', email });
  },
  getAdminById(id) {
    return this.findOne({ role: 'admin', uuid: id });
  },
  getAdminByEmail(email) {
    return this.findOne({ role: 'admin', email });
  },
  getByEmail(email) {
    return this.findOne({ email });
  },
};

module.exports = mongoose.model('User', userSchema);
