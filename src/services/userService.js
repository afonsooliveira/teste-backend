const bcrypt = require('bcrypt-nodejs');
const jwt = require('jsonwebtoken');
const User = require('../models/User');
const config = require('../config');

exports.attempt = async (email, password) => {
  const user = await User.getByEmail(email);

  if (user === null || await bcrypt.compareSync(password, user.password)) {
    return false;
  }

  return true;
};

exports.generateToken = async (email) => {
  const user = await User.getByEmail(email);

  if (user === null) {
    return false;
  }

  const token = jwt.sign({ id: user.uuid }, config.secret_key, {
    expiresIn: config.jwt_ttl,
  });

  return token;
};
