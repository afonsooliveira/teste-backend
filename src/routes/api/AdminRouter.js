const express = require('express');
const authMiddleware = require('../../middlewares/authMiddleware');
const validatorMiddleware = require('../../middlewares/validatorMiddleware');
const adminMiddleware = require('../../middlewares/adminMiddleware');
const adminController = require('../../controllers/adminController');
const adminValidation = require('../../validations/adminValidation');

const adminRouter = express.Router();
adminRouter.use(authMiddleware);
adminRouter.use(adminMiddleware);
adminRouter.get('/:id', validatorMiddleware, adminController.show);
adminRouter.post('/', adminValidation.create, validatorMiddleware, adminController.create);
adminRouter.post('/:id', adminValidation.update, validatorMiddleware, adminController.update);
adminRouter.post('/:id/delete', adminValidation.delete, validatorMiddleware, adminController.delete);

module.exports = adminRouter;
