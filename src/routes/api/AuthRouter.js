const express = require('express');
const validatorMiddleware = require('../../middlewares/validatorMiddleware');
const authValidation = require('../../validations/authValidation');
const authController = require('../../controllers/authController');

const authRouter = express.Router();
authRouter.post('/', authValidation.login, validatorMiddleware, authController.login);

module.exports = authRouter;
