const express = require('express');
const authMiddleware = require('../../middlewares/authMiddleware');
const validatorMiddleware = require('../../middlewares/validatorMiddleware');
const adminMiddleware = require('../../middlewares/adminMiddleware');
const userMiddleware = require('../../middlewares/userMiddleware');
const movieController = require('../../controllers/movieController');
const movieValidation = require('../../validations/movieValidation');

const movieRouter = express.Router();
movieRouter.use(authMiddleware);
movieRouter.get('/', validatorMiddleware, movieController.get);
movieRouter.post('/', adminMiddleware, movieValidation.create, validatorMiddleware, movieController.create);
movieRouter.post('/:id/vote', userMiddleware, movieValidation.vote, validatorMiddleware, movieController.vote);

module.exports = movieRouter;
