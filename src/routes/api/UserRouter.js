const express = require('express');
const authMiddleware = require('../../middlewares/authMiddleware');
const validatorMiddleware = require('../../middlewares/validatorMiddleware');
const adminMiddleware = require('../../middlewares/adminMiddleware');
const userController = require('../../controllers/userController');
const userValidation = require('../../validations/userValidation');

const userRouter = express.Router();
userRouter.use(authMiddleware);
userRouter.use(adminMiddleware);
userRouter.get('/:id', userValidation.show, validatorMiddleware, userController.show);
userRouter.post('/', userValidation.create, validatorMiddleware, userController.create);
userRouter.post('/:id', userValidation.update, validatorMiddleware, userController.update);
userRouter.post('/:id/delete', userValidation.delete, validatorMiddleware, userController.delete);

module.exports = userRouter;
