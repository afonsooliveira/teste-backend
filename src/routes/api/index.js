const express = require('express');
const authRouter = require('./AuthRouter');
const adminRouter = require('./AdminRouter');
const userRouter = require('./UserRouter');
const movieRouter = require('./MovieRouter');

const router = express.Router();

// Guest route
router.use('/auth', authRouter);

// Admin routes
router.use('/admin', adminRouter);

// User routes
router.use('/user', userRouter);

// Movie routes
router.use('/movie', movieRouter);

module.exports = router;
