module.exports = {
  app_name: process.env.APP_NAME,
  port: process.env.PORT,
  hostname: process.env.HOSTNAME,
  secret_key: process.env.SECRET_KEY,
  jwt_ttl: process.env.JWT_TTL,
  mongo: {
    host: process.env.MONGO_HOST,
    port: process.env.MONGO_PORT,
    user: process.env.MONGO_USER,
    password: process.env.MONGO_PASSWORD,
    database: process.env.MONGO_DATABASE,
  },
};
