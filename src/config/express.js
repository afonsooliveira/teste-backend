const express = require('express');
const cors = require('cors');
const swaggerUi = require('swagger-ui-express');
const authorizationErrorMiddleware = require('../middlewares/authorizationErrorMiddleware');
const swaggerDocument = require('./swagger.json');
const config = require('./index');
const apiRoutes = require('../routes/api');

const app = express();

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(authorizationErrorMiddleware);

app.use('/api', apiRoutes);
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

exports.start = () => {
  app.listen(config.port);
};

exports.app = app;
