const mongoose = require('mongoose');

const config = require('.');

mongoose.Promise = require('bluebird');

exports.connect = () => {
  const {
    user,
    password,
    host,
    port,
    database,
  } = config.mongo;

  mongoose.connect(`mongodb://${user}:${password}@${host}:${port}/${database}?authSource=admin`, {
    keepAlive: 1,
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });

  mongoose.set('useCreateIndex', true);

  return mongoose.connection;
};
