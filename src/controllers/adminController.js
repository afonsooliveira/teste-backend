const httpStatus = require('http-status');
const User = require('../models/User');

exports.show = async (req, res, next) => {
  try {
    const admin = await User.findOne({ role: 'admin', uuid: req.params.id });

    if (admin === null) {
      return res.status(httpStatus.NOT_FOUND).json({
        error: true,
        message: 'Admin não encontrado',
      });
    }

    return res.json(admin);
  } catch (error) {
    return next(error);
  }
};

exports.create = async (req, res, next) => {
  try {
    const { name, email, password } = req.body;
    const admin = new User({
      name,
      email,
      password,
      role: 'admin',
    });

    const savedAdmin = await admin.save();

    res.status(httpStatus.CREATED);
    return res.json(savedAdmin);
  } catch (error) {
    return next(error);
  }
};

exports.update = async (req, res, next) => {
  try {
    const { name, password } = req.body;

    const admin = await User.findOne({ role: 'admin', uuid: req.params.id });
    admin.name = name;

    if (password) {
      admin.password = password;
    }

    const updatedAdmin = await admin.save();

    return res.json(updatedAdmin);
  } catch (error) {
    return next(error);
  }
};

exports.delete = async (req, res, next) => {
  try {
    const admin = await User.findOne({ role: 'admin', uuid: req.params.id });
    const deletedAdmin = await admin.delete();

    return res.json(deletedAdmin);
  } catch (error) {
    return next(error);
  }
};
