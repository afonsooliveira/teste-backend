const userService = require('../services/userService');

exports.login = async (req, res, next) => {
  try {
    if (userService.attempt(req.body.email, req.body.password) === false) {
      throw new Error('Email ou senha incorreta');
    }

    const token = await userService.generateToken(req.body.email);
    return res.json({ message: 'OK', token });
  } catch (error) {
    return next(error);
  }
};
