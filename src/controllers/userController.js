const httpStatus = require('http-status');
const User = require('../models/User');

exports.show = async (req, res, next) => {
  try {
    return res.json(await User.getUserById(req.params.id));
  } catch (error) {
    return next(error);
  }
};

exports.create = async (req, res, next) => {
  try {
    const { name, email, password } = req.body;
    const user = new User({
      name,
      email,
      password,
      role: 'user',
    });

    const savedUser = await user.save();

    res.status(httpStatus.CREATED);
    return res.json(savedUser);
  } catch (error) {
    return next(error);
  }
};

exports.update = async (req, res, next) => {
  try {
    const { name, password } = req.body;

    const user = await User.findOne({ uuid: req.params.id });
    user.name = name;

    if (password) {
      user.password = password;
    }

    const updatedUser = await user.save();

    return res.json(updatedUser);
  } catch (error) {
    return next(error);
  }
};

exports.delete = async (req, res, next) => {
  try {
    const user = await User.findOne({ uuid: req.params.id });
    const deletedUser = await user.delete();

    return res.json(deletedUser);
  } catch (error) {
    return next(error);
  }
};
