const httpStatus = require('http-status');
const Movie = require('../models/Movie');

exports.create = async (req, res, next) => {
  try {
    const {
      name,
      synopsis,
      director,
      genre,
      actors,
    } = req.body;

    const movie = new Movie({
      name,
      synopsis,
      director,
      genre,
      actors,
    });

    const savedMovie = await movie.save();

    res.status(httpStatus.CREATED);
    return res.json(savedMovie);
  } catch (error) {
    return next(error);
  }
};

exports.get = async (req, res) => {
  const filters = {};

  if (req.query.director) {
    filters.director = req.query.director;
  }

  if (req.query.name) {
    filters.name = req.query.name;
  }

  if (req.query.genre) {
    filters.genre = req.query.genre;
  }

  if (req.query.actors) {
    filters.actors = req.query.actors;
  }

  const movies = await Movie.find(filters);

  return res.json(movies);
};

exports.vote = async (req, res) => {
  const movie = await Movie.findOne({ uuid: req.params.id });

  movie.votes.push({
    user_id: req.user.uuid,
    vote: req.body.vote,
  });

  const votedMovie = await movie.save();

  return res.json(votedMovie);
};

exports.show = async (req, res) => {
  const movie = await Movie.findOne({ uuid: req.params.id });

  const movieShow = movie.toObject();
  const totalVotes = movieShow.votes.reduce((total, vote) => total + (vote.vote || 0), 0);
  movieShow.averageVotes = totalVotes / movieShow.votes.length;

  return res.json(movieShow);
};
