const httpStatus = require('http-status');

module.exports = (req, res, next) => {
  if (req.user.role === 'admin') {
    return next();
  }

  res.status(httpStatus.FORBIDDEN);
  throw new Error('Sem permissão');
};
