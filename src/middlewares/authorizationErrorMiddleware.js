const httpStatus = require('http-status');

module.exports = (err, req, res, next) => {
  if (err.name === 'UnauthorizedError') {
    return res.status(httpStatus.UNAUTHORIZED).send({
      error: true,
      message: 'Unauthorized',
    });
  }

  return next();
};
