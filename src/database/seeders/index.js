require('dotenv').config();

const seeder = require('mongoose-seed');
const config = require('../../config');
const adminSeeder = require('./adminSeeder');

const {
  user,
  password,
  host,
  port,
  database,
} = config.mongo;

seeder.connect(`mongodb://${user}:${password}@${host}:${port}/${database}?authSource=admin`, () => {
  seeder.loadModels(['src/models/User']);

  seeder.populateModels([
    adminSeeder,
  ], () => {
    seeder.disconnect();
  });
});
