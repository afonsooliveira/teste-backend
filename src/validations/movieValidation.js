const { body, param } = require('express-validator');
const Movie = require('../models/Movie');

module.exports = {
  create: [
    body('name').notEmpty().withMessage('Nome não informado'),
    body('synopsis').notEmpty().withMessage('Sinopse não informado'),
    body('director').notEmpty().withMessage('Diretor não informado'),
    body('genre').notEmpty().withMessage('Gênero não informado'),
    body('actors').isArray().withMessage('Atores não informados'),
  ],
  vote: [
    param('id').custom(async (id) => {
      if (await Movie.getMovieById(id) === null) {
        throw new Error('Filme não encontrado');
      }
    }),
    body('vote').notEmpty().withMessage('Voto não pode ser vazio'),
    body('vote').isInt({ min: 0, max: 4 }).withMessage('Voto deve contér valor entre 0 e 4'),
  ],
};
