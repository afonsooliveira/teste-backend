const { body, param } = require('express-validator');
const User = require('../models/User');

module.exports = {
  show: [
    param('id').custom(async (id) => {
      if (await User.getUserById(id) === null) {
        throw new Error('Usuário não encontrado');
      }
    }),
  ],
  create: [
    body('name').notEmpty().withMessage('Nome não informado'),
    body('email').notEmpty().withMessage('Email não informado'),
    body('email').isEmail().withMessage('Email inválido'),
    body('email').custom(async (email) => {
      if (await User.getByEmail(email) !== null) {
        throw new Error('Email já em uso');
      }
    }),
    body('password').notEmpty().withMessage('Senha não informada'),
  ],
  update: [
    param('id').custom(async (id) => {
      if (await User.getUserById(id) === null) {
        throw new Error('Usuário não encontrado');
      }
    }),
    body('name').notEmpty().withMessage('Nome não informado'),
    body('password').isString().withMessage('Senha com formato incorreto').optional(),
  ],
  delete: [
    param('id').custom(async (id) => {
      if (await User.getUserById(id) === null) {
        throw new Error('Usuário não encontrado');
      }
    }),
  ],
};
