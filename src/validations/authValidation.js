const { body } = require('express-validator');

module.exports = {
  login: [
    body('email').notEmpty().withMessage('Email deve ser informado'),
    body('email').isEmail().withMessage('Email inválido'),
    body('password').notEmpty().withMessage('Senha não informada'),
  ],
};
