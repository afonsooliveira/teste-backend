require('dotenv').config();

const app = require('./config/express');
const mongoose = require('./config/mongoose');

app.start();
mongoose.connect();

module.exports = app;
